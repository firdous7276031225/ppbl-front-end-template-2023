import { Flex, Box, Heading, Text } from "@chakra-ui/react";
import * as React from "react";
import { StatusBox } from "@/src/components/lms/Status/StatusBox";
import { useAddress, useAssets } from "@meshsdk/react";
import { NativeScript, resolveNativeScriptHash, resolvePaymentKeyHash } from "@meshsdk/core";
import { useLazyQuery } from "@apollo/client";
import { contributorTokenPolicyId } from "@/src/cardano/plutus/contributorPlutusMintingScript";
import { PPBLContext } from "@/src/context/PPBLContext";
import { GraphQLTransactionMetadata } from "@/src/types/cardanoGraphQL";
import { ADDRESS_METADATA_QUERY } from "@/src/data/queries/metadataQueries";
import Image from "next/image";

type Props = {
  children?: React.ReactNode;
};

const Status203: React.FC<Props> = ({ children }) => {
  const walletAssets = useAssets();
  const address = useAddress(0);

  const ppblContext = React.useContext(PPBLContext);

  const [lesson2031metadata, setLesson203metadata] = React.useState<GraphQLTransactionMetadata[] | undefined>(undefined)
  const [ppblNFT, setPPBLNFT] = React.useState(false)

  const [metadataQuery, { data: metadataQueryData }] = useLazyQuery(ADDRESS_METADATA_QUERY);


  React.useEffect(() => {
    // Module 203.1: Metadata at key 2023
    // Does not check for Mesh/GameChanger completion of metadata tx
    if (ppblContext.cliAddress) {
      metadataQuery({
        variables: {
          walletAddress: ppblContext.cliAddress,
          metadataKey: "2023",
        },
      });
    }


    // Module 203.2: PPBL NFT
    if(walletAssets){
      setPPBLNFT(walletAssets.some((a) => a.unit.substring(0, 56) == "2a384dc205a97463577fc98b704b537f680c0eba84126eb7d5857c86"))
    }
  
  }, [ppblContext.cliAddress, metadataQuery, walletAssets]);

  React.useEffect(() => {
    if (metadataQueryData && metadataQueryData.transactions.length > 0) {
      const _metadata: GraphQLTransactionMetadata[] = []
      metadataQueryData.transactions.forEach((tx: any) => {
        _metadata.push(tx.metadata[0])
      })
      setLesson203metadata(_metadata)
    }
  }, [metadataQueryData]);


  return (
    <>
      <Flex direction="row" justifyContent="center" alignItems="center">
        <StatusBox condition={lesson2031metadata != undefined} text="203.1: Add Transaction Metadata" />
        <StatusBox condition={ppblNFT} text="203.2: Mint a PPBL NFT" />
        <StatusBox condition={false} text="203.3: Read CIPs" />
        <StatusBox condition={false} text="203.4: Build PPBL 2023 Token" />
      </Flex>
    </>
  );
};

export default Status203;
