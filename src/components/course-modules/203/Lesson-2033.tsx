import LessonLayout from "@/src/components/lms/Lesson/LessonLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "./module203.json";
import Docs2033 from "@/src/components/course-modules/203/Docs2033.mdx";

export default function Lesson2033() {
  const slug = "2033";
  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <LessonLayout moduleNumber={203} sltId="203.3" slug={slug}>
      <LessonIntroAndVideo lessonData={lessonDetails} />
      <Docs2033 />
    </LessonLayout>
  );
}
