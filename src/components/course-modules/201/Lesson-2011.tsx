import LessonLayout from "@/src/components/lms/Lesson/LessonLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "./module201.json";
import Docs2011 from "@/src/components/course-modules/201/Docs2011.mdx";

export default function Lesson2011() {
  const slug = "2011";
  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <LessonLayout moduleNumber={201} sltId="201.1" slug={slug}>
      <LessonIntroAndVideo lessonData={lessonDetails} />
      <Docs2011 />
    </LessonLayout>
  );
}
