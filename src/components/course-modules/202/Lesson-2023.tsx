import LessonLayout from "@/src/components/lms/Lesson/LessonLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "./module202.json";
import Docs2023 from "@/src/components/course-modules/202/Docs2023.mdx";

export default function Lesson2023() {
  const slug = "2023";
  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <LessonLayout moduleNumber={202} sltId="202.3" slug={slug}>
      <LessonIntroAndVideo lessonData={lessonDetails} />
      <Docs2023 />
    </LessonLayout>
  );
}
