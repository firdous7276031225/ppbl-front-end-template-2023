import LessonLayout from "@/src/components/lms/Lesson/LessonLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "./module202.json";
import Docs2025 from "@/src/components/course-modules/202/Docs2025.mdx";

export default function Lesson2025() {
  const slug = "2025";
  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <LessonLayout moduleNumber={202} sltId="202.5" slug={slug}>
      <LessonIntroAndVideo lessonData={lessonDetails} />
      <Docs2025 />
    </LessonLayout>
  );
}
