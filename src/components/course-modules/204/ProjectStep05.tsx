import ProjectLayout from "@/src/components/lms/Lesson/ProjectLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "./module204.json";
import DocsStep05 from "@/src/components/course-modules/204/DocsStep05.mdx";

export default function ProjectStep05() {
  const slug = "project-step-05";
  const title = "Step 5: Use Metadata to Register with Faucet Aggregator"

  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <ProjectLayout moduleNumber={204} title={title} sltId="203.1" slug={slug}>
      <DocsStep05 />
    </ProjectLayout>
  );
}
